package datastruct;

import org.junit.*;
import static org.junit.Assert.*;

public class MyUnsortedListTest {

		public MyUnsortedListTest() {}
		
		/*
		 * Test de la m�thode popLast
		 */
		@Test
		public void testPopLast_Taille() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Retire le dernier element de la liste", (Integer) 3, list.popLast());
			assertEquals("Taille liste -1", 2, list.size());
		}
		
		@Test(expected = EmptyListException.class)
		public void testPopLast_ListeVide() throws Exception {
			UnsortedList<Integer> list = MyUnsortedList.of();
			list.popLast();
		}
		
		@Test
		public void testPopLast_Successif() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Suppression de l'element � la fin de la liste", (Integer) 3, list.popLast());
			assertEquals("Suppression de l'element � la fin de la liste (apres un popLast)", (Integer) 2, list.popLast());
			assertEquals("Suppression de l'element � la fin de la liste (apres deux popLast)", (Integer) 1, list.popLast());
		}
		
		/*
		 * Test de la m�thode pop
		 */
		@Test
		public void testPop_Taille() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Suppression de l'element � l'emplacement 0 de la liste", (Integer) 1, list.pop());
			assertEquals("Taille liste -1", 2, list.size());
		}
		
		@Test(expected = EmptyListException.class)
		public void testPop_ListeVide() throws Exception {
			UnsortedList<Integer> list = MyUnsortedList.of();
			list.pop();
		}
		
		@Test
		public void testPop_Successif() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Suppression de l'element � l'emplacement 0 de la liste", (Integer) 1, list.pop());
			assertEquals("Suppression de l'element � l'emplacement 0 de la liste (apres un pop)", (Integer) 2, list.pop());
			assertEquals("Suppression de l'element � l'emplacement 0 de la liste (apres deux pop)", (Integer) 3, list.pop());
		}
		
		/*
		 * Test de la m�thode remove
		 */
		@Test(expected = IndexOutOfBoundsException.class)
		public void testRemove_ListeVide () {
			UnsortedList<Integer> list = MyUnsortedList.of();
			list.remove(0);
		}
		
		@Test(expected = IndexOutOfBoundsException.class)
		public void testRemove_IndexIncorrectSuperieur () {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.remove(3);
		}
		
		@Test(expected = IndexOutOfBoundsException.class)
		public void testRemove_IndexIncorrectInferieur () {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.remove(-1);
		}
		
		@Test
		public void testRemove_Taille() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			int elementSupprime = list.remove(1);
			assertEquals("Sup d'un element", 2, list.size());
			assertEquals("Sup element indice 1 [1,2,3]", 2, elementSupprime);
		}
		
		@Test
		public void testRemove_Debut () {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Sup element indice 0", (Integer) 1, list.remove(0));
		}
		
		@Test
		public void testRemove_Fin () {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Sup element indice size-1", (Integer) 3, list.remove(list.size()-1));
		}
		
		@Test
		public void testRemove_Successif () {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			assertEquals("Sup element indice 1 ([1,2,3])", (Integer) 2, list.remove(1));
			assertEquals("Sup element indice 1 ([1,3])", (Integer) 3, list.remove(1));
			assertEquals("Sup element indice 0 ([1])", (Integer) 1, list.remove(0));
		}
		/*
		 * Test de la m�thode append
		 */
		@Test
		public void testAppend_Ajout_Taille() {
			UnsortedList<Integer> list = MyUnsortedList.of(0,0,0);
			list.append(1);
			assertEquals("Ajout d'un element + verif de sa nouvelle taille", 4, list.size());
			assertEquals("Ajout d'un element + verif de sa pr�sence en fin de liste", (Integer) 1, list.popLast());
		}

		@Test
		public void testAppend_Successif() {
			UnsortedList<Integer> list = MyUnsortedList.of(0);
			list.append(1);
			list.append(2);
			assertEquals("Ajout d'elements + verif de la pr�sence du dernier ajout� en fin de liste", (Integer) 2, list.popLast());
			list.append(3);
			assertEquals("Ajout d'elements + verif de la pr�sence du dernier ajout� en fin de liste", (Integer) 3, list.popLast());
		}
		
		/*
		 * Test de la m�thode prepend
		 */
		@Test
		public void testPrepend_Ajout_Taille() {
			UnsortedList<Integer> list = MyUnsortedList.of(0,0,0);
			list.prepend(1);
			assertEquals("Ajout d'un element + verif de sa nouvelle taille", 4, list.size());
			assertEquals("Ajout d'un element + verif de sa pr�sence en fin de liste", (Integer) 1, list.pop());
		}
		
		@Test
		public void testPrepend_Successif() {
			UnsortedList<Integer> list = MyUnsortedList.of(0);
			list.prepend(1);
			list.prepend(2);
			assertEquals("Ajout d'elements + verif de la pr�sence du dernier ajout� en d�but de liste", (Integer) 2, list.pop());
			list.prepend(3);
			assertEquals("Ajout d'elements + verif de la pr�sence du dernier ajout� en d�but de liste", (Integer) 3, list.pop());
		}
		
		/*
		 * Test de la m�thode insert
		 */
		@Test(expected = IndexOutOfBoundsException.class)
		public void testInsert_FinErreur() {
			UnsortedList<Integer> list = MyUnsortedList.of();
			list.insert(1, 1);
		}
		
		@Test(expected = IndexOutOfBoundsException.class)
		public void testInsert_DebutErreur() {
			UnsortedList<Integer> list = MyUnsortedList.of();
			list.insert(1, -1);
		}
		
		@Test
		public void testInsert_Debut() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.insert(0, 0);
			assertEquals("Ajout d'un element + verif de sa nouvelle taille", 4, list.size());
			assertEquals("Ajout d'elements � l'indice 0", (Integer) 0, list.pop());
		}
		
		@Test
		public void testInsert_Fin() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.insert(4, 3);
			assertEquals("Ajout d'un element + verif de sa nouvelle taille", 4, list.size());
			assertEquals("Ajout d'elements � l'indice 3", (Integer) 4, list.popLast());
		}
		
		@Test
		public void testInsert_Milieu() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.insert(0, 1);
			assertEquals("Ajout d'un element + verif de sa nouvelle taille", 4, list.size());
			list.pop();//On cherche le 2e element
			assertEquals("Ajout d'elements � l'indice 1", (Integer) 0, list.pop());
		}
		
		@Test
		public void testInsert_Pop_Successif() {
			UnsortedList<Integer> list = MyUnsortedList.of(1,2,3);
			list.insert(0, 1);
			list.insert(0, 4);
			list.insert(0, 2);
			assertEquals("Ajout de 3 elements", 6, list.size());
			assertEquals("Verification element d'indice 0", (Integer) 1, list.pop());
			assertEquals("Verification element d'indice 1", (Integer) 0, list.pop());
			assertEquals("Verification element d'indice 2", (Integer) 0, list.pop());
			assertEquals("Verification element d'indice 3", (Integer) 2, list.pop());
			assertEquals("Verification element d'indice 4", (Integer) 3, list.pop());
			assertEquals("Verification element d'indice 5", (Integer) 0, list.pop());
		}
		
		/*
		 * Test de la m�thode isEmpty
		 */
		@Test
		public void testIsEmpty_Remove() {
			UnsortedList<Integer> list = MyUnsortedList.of();
			assertTrue("Liste : vide", list.isEmpty());
			list.append(1);
			assertFalse("Liste : 1 elements ajoute", list.isEmpty());
			list.remove(0);
			assertTrue("Liste : 1 element supprime", list.isEmpty());
		}
		
		@Test
		public void testIsEmpty_Pop() {
			UnsortedList<Integer> list = MyUnsortedList.of(0);
			list.pop();
			assertTrue("Liste : 1 element supprime", list.isEmpty());
		}
		
		@Test
		public void testIsEmpty_PopLast() {
			UnsortedList<Integer> list = MyUnsortedList.of(0);
			list.popLast();
			assertTrue("Liste : 1 element supprime", list.isEmpty());
		}
		
		/*
		 * Test de la m�thode size 
		 */
		@Test
		public void testSize() {
			UnsortedList<Integer> list = MyUnsortedList.of();
			assertEquals("Liste : vide", 0, list.size());
			list.append(1);
			assertEquals("Liste : 1 element", 1, list.size());
			list.append(2);
			list.append(3);
			list.append(4);
			assertEquals("Liste [1,2,3,4] : 4 elements", 4, list.size());
			list.remove(0);
			assertEquals("Liste [2,3,4]: 4-1 elements", 3, list.size());
			list.remove(1);
			assertEquals("Liste [2,4]: 4-1-1 elements", 2, list.size());
		}
}
